import * as mongoose from 'mongoose';

export const postSchema = new mongoose.Schema({
    Title: String,
    Author: String,
    Url: String,
    PostDate: Date,
    Status: Boolean
}, {
    versionKey: false
});  