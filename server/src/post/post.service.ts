import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from './post.model';

interface PostInterface {
    id: string;
}

@Injectable()
export class PostService {
    constructor(@InjectModel('Posts') private readonly postModel: Model<Post>) { }

    async getAll() {
        return await this.postModel.find();
    }
    async add(post: PostInterface) {
        const createPost = new this.postModel(post);
        await createPost.save();
    }
    async update(post: PostInterface) {
        await this.postModel.updateOne({ _id: post.id }, post);
    }
    async delete(id: string) {
        await this.postModel.deleteOne({ _id: id });
    }
}