import { Controller, Get, Post, Body, Delete, Param } from '@nestjs/common';
import { PostService } from './post.service';

@Controller('posts')
export class PostController {
    constructor(private service: PostService) { }

    @Get('')
    async GetAll() {
        return await this.service.getAll();
    }
    @Post('add')
    async Add(@Body() post: any) {
        await this.service.add(post);
    }
    @Post('update')
    async Update(@Body() post: any) {
        await this.service.update(post);
    }
    @Delete('delete/:id')
    async Delete(@Param('id') id: string) {
        await this.service.delete(id);
    }
}