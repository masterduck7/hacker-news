import React, { Component } from 'react';
import './home.css';
import Posts from '../posts/posts';

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <>
        <div className="banner">
          <h1 className="title">HN Feed</h1>
          <h3 className="subtitle">We love Hacker News</h3>
        </div>
        <Posts />
      </>
    )
  }
}