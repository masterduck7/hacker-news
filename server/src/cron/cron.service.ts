import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import axios from 'axios';

@Injectable()
export class CronService {
    @Cron('0 * * * * *')
    async getDataEveryHour() {
        await axios.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').then(response => {
            try {
                console.log("OK")
            } catch (error) {
                console.log("Error when trying to get data. Error: " + error)
            }
        })
    }
}
