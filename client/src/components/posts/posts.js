import { Component } from 'react';
import axios from 'axios';
import './posts.css';

export default class Posts extends Component {
  constructor(props) {
    super(props)
    this.state = {
      posts: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3005/posts')
      .then(res => {
        if (!res.data["Error"]) {
          var news = []
          res.data.forEach(post => {
            if (post.Status) {
              news.push(post)
            }
          });
          this.setState({
            posts: news
          })
        } else {
          console.log("Error in Get news form server")
        }
      })
  }

  deactivatePost(id) {
    var post = {
      id: id,
      Status: 0
    }
    axios.post('http://localhost:3005/posts/update', post)
      .then(function (response) {
        alert("News removed")
        window.location.reload();
      })
      .catch(function (error) {
        console.log("Error in remove news ", error);
        alert("This news can't be removed")
      });
  }

  render() {
    return (
      <div className="posts">
        <ul className="nobull">
          {
            this.state.posts.map((post, i) =>
              <li key={i}>
                <button className="post" onClick={(e) => {
                  window.open(post.Url, "_blank");
                }}>
                  <b className="text">{post.Title}</b> - <b className="author text">{post.Author}</b>
                  <b className="date">{post.PostDate.substring(0, 10)}</b>
                </button>
                <button onClick={(e) => this.deactivatePost(post._id)} className="action text">Remove</button>
              </li>
            )
          }
        </ul>
      </div>
    )
  }
}