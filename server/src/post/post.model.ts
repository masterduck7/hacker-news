import * as mongoose from 'mongoose';

export interface Post extends mongoose.Document {
    Title: String,
    Author: String,
    Url: String,
    PostDate: Date,
    Status: Boolean
} 