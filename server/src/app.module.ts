import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';

import { postSchema } from './post/post.schema';
import { PostController } from './post/post.controller';
import { PostService } from './post/post.service';
import { CronService } from './cron/cron.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ScheduleModule.forRoot(),
    MongooseModule.forRoot("mongodb://mongo/reign"),
    MongooseModule.forFeature([{
      name: 'Posts',
      schema: postSchema,
      collection: 'Posts'
    }]),
  ],
  controllers: [AppController, PostController],
  providers: [AppService, PostService, CronService],
})
export class AppModule { }
